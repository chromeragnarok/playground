package main

import (
	"github.com/gliderlabs/ssh"
	"io"
	"log"
	"os/exec"
	"strings"
	"fmt"
)

func main() {
	ssh.Handle(func(s ssh.Session) {
		remoteCmd := s.Command()
		fmt.Println(remoteCmd)
		if remoteCmd != nil {
			io.WriteString(s, "Hello world\n")
			out, err := exec.Command(strings.Join(remoteCmd, " ")).Output()
			if err != nil {
				s.Write([]byte(err.Error()))
			} else {
				s.Write(out)
			}
		} else {
			io.WriteString(s, "Hello planet\n")
			log.Fatal("what is sthis")
		}
	})

	log.Fatal(ssh.ListenAndServe(":2222", nil))
}
